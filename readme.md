### Quale Project - Laravel Learning Management System

Live Demo :
[Not Available yet](#)

## Main view

![Main view](https://gdurl.com/kPE8)

## Student - Front view 

![Student - Front view](https://gdurl.com/oYSc)

## Admin - Panel view

Credentials to log in:

Email: admin@admin.com

Password: password

![Admin - Panel view](https://gdurl.com/APb1)

### License
Please use and re-use however you want.

# Quale Team : 

- [Muhammad Iskandar Dzulqornain](#)
- [Julio Alfian Dwi](#)
- [Intan Irnanda](#)
- [Muhammad Fajar M](#)
- [Ahmad Baihaqi](#)